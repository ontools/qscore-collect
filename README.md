# README

`qscore-collect` finds and summarizes Q-scores from sequence summary files
from MinKNOW(live base calling) and/or guppy base calling runs.

It searches recursively for the sequence summary files of a given directory to
find sequence summaries containing base calling data.

## Install

`git clone https://gitlab.com/ontools/qscore-collect.git`

## Usage

`qscore-collect/src/ont-qscore.py -d <rootdir>`

Example:

`$HOME/qscore-collect/src/ont-qscore.py -d /path/to/sequencing/directory > qscore.tsv`

If no path is given, the current working directory is screened.

## Output

Write tab delimited fields to STDOUT:

| column | name            | description                           |
| ------ | --------------- | ------------------------------------- |
| 1      | `file`          | summary                               |
| 2      | `reads`         | number of total reads                 |
| 3      | `reads_fail`    | number of failed reads                |
| 4      | `reads_pass`    | number of passed                      |
| 5      | `qscore_mean`   | Mean qscore value from passed reads   |
| 6      | `qscore_median` | Median qscore value from passed reads |
| 7      | `qscore_min`    | Lowest qscore for passed reads        |
| 8      | `qscore_max`    | highest qscore for passed reads       |

Run information is printed tab-delimited to STDERR:

- summary path
- expected column names and their index within the summary file
- Parsing Status: OK or skip/error (reason when known)
