#!/usr/bin/env python3
"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import os
import sys
import statistics
import argparse


class SummaryFinder:

  def __init__(self) -> None:
    self.skipdirs = set(['fast5_fail', 'fast5_pass', 'fastq_fail', 'fastq_pass', 'fail', 'pass'])
    print('\t'.join(SummaryParser.header))
  def find_summaries(self, rootdir):
    if os.access(rootdir, os.R_OK):
      for i in os.scandir(rootdir):
        if os.path.isdir(i) and i.name not in self.skipdirs:
          self.find_summaries(i)
        if os.path.isfile(i) and i.name.startswith('sequencing_summary'):
          sp = SummaryParser()
          sp.parse(os.path.abspath(i))
    else:
      print(f"Warning: Skipping {rootdir}.No read permission.")

class SummaryParser:

  header = ['#file', 'reads', 'reads_fail', 'reads_pass', 'qscore_mean', 'qscore_median', 'qscore_min', 'qscore_max']

  def __init__(self) -> None:
    self.req_cols = {'passes_filtering':0, 'mean_qscore_template':0}
    self.isHeader = True
    self.header = ['#file', 'reads', 'reads_fail', 'reads_pass', 'qscore_mean', 'qscore_median', 'qscore_min', 'qscore_max']

  def init(self, summary, header):
    self.isHeader = True
    colnum = 0
    colstat = {}
    for i in header:
      if i in self.req_cols:
        self.req_cols[i] = colnum
      colnum += 1
    print(f"{summary}", end="\t", file=sys.stderr)
    for i in self.req_cols:
      if self.req_cols[i] == 0:
        print(f"{i}:-1\tskip", file=sys.stderr)
        return False
      print(f"{i}:{self.req_cols[i]+1}", end="\t", file=sys.stderr)
    self.isHeader = False
    return True


  def column(self, cols, colname):
    return cols[self.req_cols[colname]]

  def parse(self, summary):
    qscores = []
    failed = 0
    total = 0
    fh = open(summary, 'r')
    for i in fh:
      cols = i.strip().split('\t')
      if self.isHeader:
        if not self.init(summary, cols):
          return
      if self.column(cols, 'passes_filtering') == 'TRUE':
        qscores.append(float(self.column(cols, 'mean_qscore_template')))
      else:
        failed += 1
      total += 1
    fh.close()
    if (len(qscores) + failed) != total:
      print(f"Error for {summary}: Readnumber does not add up::Pass:{len(qscores)}\tFail:{failed}\tTot:{total}\tskip", file=sys.stderr)
      return
    print('\t'.join(str(x) for x in [summary, total, failed, len(qscores), statistics.mean(qscores), statistics.median(qscores), min(qscores), max(qscores)]))
    print("OK", file=sys.stderr)
def main():
  ap = argparse.ArgumentParser(description='Collect ONT qscores')
  ap.add_argument('-d', '--dir', default=os.getcwd(),
                  help="Root directory to search and parse for sequencing summaries.")
  args = ap.parse_args()
  sf = SummaryFinder()
  sf.find_summaries(args.dir)
  return 0

if __name__ == '__main__':
  main()
